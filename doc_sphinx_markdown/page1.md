# Markdown Sphinx page

This pages uses normal markdown syntax.
Only HTML output is possible, and basically,
the only improvement of sphinx+markdown over just markdown alone,
is that one can use the sphinx themes.

## Warning

Markdown doesn’t support a lot of the features of Sphinx,
like inline markup and directives.
However, it works for basic prose content.

reStructuredText is the preferred format for technical documentation,
please read [this blog post](http://ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/)
for motivation.

